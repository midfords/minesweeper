#pragma once
#include "Colours.h"

class Cell
{
public:
	Cell();
	Cell(int x, int y);
	bool hasMine();
	void setVisible(bool value);
	bool isVisible();
	bool isEmpty();
	void setHasMine(bool value);
	bool successfullyMarked();
	void select();
	void deselect();
	void toggleFlag();
	void toggleMineMarker();
	void setNumberOfAdjacentMines(int value);
	int getNumberOfAdjacentMines();
	~Cell(void);
private:
	// Variables
	bool mine;
	bool visible;
	bool extend;
	char character;
	int numberOfAdjacentMines;
	int x;
	int y;
	fore_colour foreColour;
	back_colour backColour;

	// Functions
	void adjustCellColour(fore_colour fc, back_colour bc);
	void adjustCell(char symbol, fore_colour foreColour, back_colour backColour, bool extend);
	void text_colour(fore_colour fc, back_colour bc);
};

