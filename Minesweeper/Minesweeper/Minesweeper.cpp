// Minesweeper.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Gameboard.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	char key;
	int gameover = 0;
	Gameboard *game = new Gameboard(50);

	game->drawBoard();

	while (gameover == 0)
	{
		while(!_kbhit()) { }
		key = _getch();
		if ((int)key == -32)
		{
			switch(_getch())
			{
			case 72: game->sendCommand('u');
				break;
			case 80: game->sendCommand('d');
				break;
			case 75: game->sendCommand('l');
				break;
			case 77: game->sendCommand('r');
				break;
			}
		} else if (key == 'f')
		{
			game->sendCommand('f');
		} else if (key == 'm')
		{
			gameover = game->sendCommand('m');
		} else if (key == '\r')
		{
			gameover = game->sendCommand('\r');
		}
	}

	if (gameover == -1) 
	{
		game->text_colour(f_black, b_red);
		cout<<"Game Over. "<<endl;
	}
	else 
	{
		game->text_colour(f_black, b_green);
		cout<<"Congratulations! "<<endl;
	}
	game->text_colour(f_grey, b_black);
	_getch();

	return 0;
}