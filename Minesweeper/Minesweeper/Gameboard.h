#pragma once
#include "Cell.h"

class Gameboard
{
public:
	Gameboard(int numMines);
	int sendCommand(char command);
	void drawBoard();
	void text_colour(fore_colour fg, back_colour bg);
	~Gameboard(void);
private:
	static const int X = 20;
	static const int Y = 20;
	Cell board [X][Y];
	int selectedX;
	int selectedY;
	int executeMove();
	bool checkGameWon();
	void cascadeBoard(int currX, int currY, bool lastnum);
	void seedRandomNumberGenerator(void);
};
