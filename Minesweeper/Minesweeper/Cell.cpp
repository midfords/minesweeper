#include "stdafx.h"
#include "Cell.h"

using namespace std;

Cell::Cell() { }

Cell::Cell(int cx, int cy)
{
	x = cx;
	y = cy;
	foreColour = f_black;
	backColour = b_grey;
	visible = false;
	character = ' ';
	mine = false;
	numberOfAdjacentMines = 0;
}

void Cell::select()
{
	fore_colour temp1 = foreColour;
	back_colour temp2 = backColour;
	this->adjustCellColour(f_black, b_white);
	foreColour = temp1;
	backColour = temp2;
}

void Cell::deselect()
{
	this->adjustCellColour(foreColour, backColour);
}

// Function to set the text color TODO: Move to Colours.h
void Cell::text_colour(fore_colour fg, back_colour bg)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO con_info;
	GetConsoleScreenBufferInfo(hConsole, &con_info);
	const int saved_colours = con_info.wAttributes;
	SetConsoleTextAttribute(hConsole, fg | bg);
}

void Cell::toggleFlag()
{
	if (character != 'f')
		adjustCell('f', f_black, b_green, false);
	else
		adjustCell(' ', f_black, b_grey, true);
	select();
}

void Cell::toggleMineMarker()
{
	if (character != 'x')
		adjustCell('x', f_white, b_red, false);
	else
		adjustCell(' ', f_black, b_grey, true);
	select();
}

void Cell::adjustCellColour(fore_colour fc, back_colour bc)
{
	// Update colours
	foreColour = fc;
	backColour = bc;

	// Set cursor
	COORD coord = {x*3+1, y*2+1};
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

	// Change colour
	text_colour(foreColour, backColour);

	if (visible == false)
	{
		if (extend) cout<<character;
		else cout<<" ";
		cout<<character;
	}
	else
	{
		cout<<" ";
		if (numberOfAdjacentMines>0) cout<<numberOfAdjacentMines;
		else cout<<" ";
	}
}

void Cell::adjustCell(char symbol, fore_colour foreColour, back_colour backColour, bool extend)
{
	// Update properties for cell
	character = symbol;
	this->extend = extend;
	this->foreColour = foreColour;
	this->backColour = backColour;
	
	// Set cursor
	COORD coord = {x*3+1, y*2+1};
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

	// Change colour
	text_colour(foreColour, backColour);

	// Print symbol
	if (symbol != 'n')
	{	
		if (extend) cout<<symbol;
		else cout<<" ";
		cout<<symbol;
	}
	else
	{
		if (extend) cout<<numberOfAdjacentMines;
		else cout<<" ";
		cout<<numberOfAdjacentMines;
	}
	// Hide cursor
	//COORD coord2 = {0, 1000};
	//SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord2);
}

bool Cell::hasMine()
{
	return mine;
}

bool Cell::isVisible()
{
	return visible;
}

bool Cell::isEmpty()
{
	return numberOfAdjacentMines == 0 && !mine;
}

void Cell::setHasMine(bool value)
{
	mine = value;
}

bool Cell::successfullyMarked()
{
	// not visible, mine and marked mine
	if (mine)
	{
		if (character == 'x')
			return true;
		else
			return false;
	}
	else if (visible == false)
	{
		return false;
	}
	else 
	{
		return true;
	}
}

void Cell::setVisible(bool value)
{
	visible = value;
	if (visible)
	{
		if (mine && character == 'x')
		{
			adjustCell('x', f_black, b_green, false);
		}
		else if (mine)
		{
			adjustCell('x', f_white, b_red, false);
		}
		else if (numberOfAdjacentMines > 0)
		{
			fore_colour fc;
			back_colour bc;

			switch (numberOfAdjacentMines)
			{
			case 1:
				fc = f_blue;
				bc = b_black;
				break;
			case 2:
				fc = f_green;
				bc = b_black;
				break;
			case 3:
				fc = f_dred;
				bc = b_black;
				break;
			case 4:
				fc = f_dblue;
				bc = b_black;
				break;
			case 5:
				fc = f_grey;
				bc = b_black;
				break;
			case 6:
				fc = f_dgreen;
				bc = b_black;
				break;
			case 7:
				fc = f_magenta;
				bc = b_black;
				break;
			case 8:
				fc = f_dcyan;
				bc = b_black;
				break;
			}
			adjustCell('n', fc, bc, false);
		}
		else
		{
			adjustCell(' ', f_grey, b_black, character == ' ');
		}
	}
}

void Cell::setNumberOfAdjacentMines(int value)
{
	numberOfAdjacentMines = value;
}

int Cell::getNumberOfAdjacentMines()
{
	return numberOfAdjacentMines;
}

Cell::~Cell(void)
{
}
