#include "stdafx.h"
#include "Gameboard.h"

using namespace std;

Gameboard::Gameboard(int numMines)
{
	selectedX = 0;
	selectedY = 0;

	//initialize board (0)
	for (int i = 0; i < X; i++)
	{
		for (int j = 0; j<Y; j++)
		{
			board[i][j] = *new Cell(i, j);
		}
	}

	// Place mines (-1)
	int randomX;
	int randomY;
	seedRandomNumberGenerator();

	if (numMines >= X*Y || numMines <= 0)
		numMines = X*Y/2;

	for (int i = 0; i<numMines; i++)
	{
		do
		{
			randomX = rand() % X;
			randomY = rand() % Y;
		
		} while( board[randomX][randomY].hasMine() );

		board[randomX][randomY].setHasMine(true);
	}

	// Set cell numbers
	int counter;
	for (int i = 0; i<X; i++)
	{	
		for (int j = 0; j<Y; j++)
		{
			counter = 0;
			
			// Check Left
			if (i > 0 && board[i-1][j].hasMine())
				counter++;

			// Check Left/Down
			if (i > 0 && j < Y-1 && board[i-1][j+1].hasMine())
				counter++;

			// Check Down
			if (j < Y-1 && board[i][j+1].hasMine())
				counter++;

			// Check Down/Right
			if (j < Y-1 && i < X-1 && board[i+1][j+1].hasMine())
				counter++;

			// Check Right
			if (i < X-1 && board[i+1][j].hasMine())
				counter++;

			// Check Up/Right
			if (i < X-1 && j > 0 && board[i+1][j-1].hasMine())
				counter++;

			// Check Up
			if (j > 0 && board[i][j-1].hasMine())
				counter++;

			// Check up/Left
			if (j > 0 && i>0 && board[i-1][j-1].hasMine())
				counter++;

			board[i][j].setNumberOfAdjacentMines(counter);
		}
	}
}

void Gameboard::seedRandomNumberGenerator()
{
	// Initialize random seed
	srand ((int)time(NULL));
}

int Gameboard::sendCommand(char command)
{
	switch (command)
	{
	case 'u': 
		board[selectedX][selectedY].deselect();
		if (selectedY - 1 >= 0) selectedY -= 1;
		board[selectedX][selectedY].select();
		break;
	case 'd': 
		board[selectedX][selectedY].deselect();
		if (selectedY + 1 < Y) selectedY += 1;
		board[selectedX][selectedY].select();
		break;
	case 'l': 
		board[selectedX][selectedY].deselect();
		if (selectedX - 1 >= 0) selectedX -= 1;
		board[selectedX][selectedY].select();
		break;
	case 'r': 
		board[selectedX][selectedY].deselect();
		if (selectedX + 1 < X) selectedX += 1;
		board[selectedX][selectedY].select();
		break;
	case 'f': 
		if (!board[selectedX][selectedY].isVisible()) board[selectedX][selectedY].toggleFlag();
		break;
	case 'm': 
		if (!board[selectedX][selectedY].isVisible()) 
			board[selectedX][selectedY].toggleMineMarker();
		return checkGameWon();
		break;
	case '\r':
		return executeMove();
		break;
	}
	return 0; // Game continues by default
}

bool Gameboard::checkGameWon()
{			
	// Check for win
	bool notDone = false;

	for (int i = 0; i < X; i++)
		for (int j = 0 ; j <Y; j++)
			if (!board[i][j].successfullyMarked())
				notDone = true;

	return !notDone; // Game won
}

int Gameboard::executeMove()
{
	if (board[selectedX][selectedY].hasMine())
	{
		for (int i = 0; i < X; i++)
		{	for (int j = 0; j <Y; j++)
			{
				board[i][j].setVisible(true);
			}
		}
		cout<<"|";
		text_colour(f_black, b_grey);
		return -1; // Game over
	}
	else if (board[selectedX][selectedY].getNumberOfAdjacentMines()>0)
	{
		board[selectedX][selectedY].setVisible(true);
	}
	else
	{
		cascadeBoard(selectedX, selectedY, board[selectedX][selectedY].getNumberOfAdjacentMines() > 0);
	}

	board[selectedX][selectedY].select();
	return checkGameWon(); // Game continues
}

void Gameboard::cascadeBoard(int currX, int currY, bool lastCellWasNumber)
{
	if (board[currX][currY].isVisible() == false && !lastCellWasNumber)
	{
		board[currX][currY].setVisible(true);
	
		// Check Left
		if (currX > 0)
		{	
			cascadeBoard(currX-1, currY, board[currX][currY].getNumberOfAdjacentMines()>0);
		}

		// Check Down
		if (currY < Y-1)
		{	
			cascadeBoard(currX, currY+1, board[currX][currY].getNumberOfAdjacentMines()>0);
		}

		// Check Right
		if (currX < X-1)
		{	
			cascadeBoard(currX+1, currY, board[currX][currY].getNumberOfAdjacentMines()>0);
		}

		// Check Up
		if (currY > 0)
		{	
			cascadeBoard(currX, currY-1, board[currX][currY].getNumberOfAdjacentMines()>0);
		}

		// Only do a diagonal if the current cell is blank and the diagonal cell is a number
		
		if (board[currX][currY].isEmpty())
		{
			// Check Left/Down
			if (currX > 0 && currY < Y-1 && !board[currX-1][currY+1].isEmpty())
				cascadeBoard(currX-1, currY+1, false);

			// Check Down/Right
			if (currY < Y-1 && currX < X-1 && !board[currX+1][currY+1].isEmpty())
				cascadeBoard(currX+1, currY+1, false);

			// Check Up/Right
			if (currX < X-1 && currY > 0 && !board[currX+1][currY-1].isEmpty())
				cascadeBoard(currX+1, currY-1, false);

			// Check up/Left
			if (currY > 0 && currX>0 && !board[currX-1][currY-1].isEmpty())
				cascadeBoard(currX-1, currY-1, false);
		}
	}
}

void Gameboard::drawBoard()
{
	for (int i = 0; i < X*2; i++)
	{
		for (int j = 0; j < Y; j++)
		{
			if (i%2 == 0)
			{
				cout<<"+--";
			}
			else
			{
				cout<<"|";

				text_colour(f_black, b_grey);
				cout<<"  ";
				text_colour(f_grey, b_black);
			}
		}
		if (i%2 == 0)
		{
			cout<<"+"<<endl;
		}
		else
		{	
			cout<<"|"<<endl;
		}
	}

	for (int i = 0; i<Y; i++)
	{
		cout<<"+--";
	}
	cout<<"+"<<endl;

	board[selectedX][selectedY].select();
}

// Function to set the text color TODO: move to Colours.h
void Gameboard::text_colour(fore_colour fg, back_colour bg)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO con_info;
	GetConsoleScreenBufferInfo(hConsole, &con_info);
	const int saved_colours = con_info.wAttributes;
	SetConsoleTextAttribute(hConsole, fg | bg);
}

Gameboard::~Gameboard(void)
{
}
